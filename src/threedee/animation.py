# Game: "Colored Sources in Space" - csis
# Author: Plyspomitox (Contact: @xoryps)
# License: CC-by-sa
#
#

from PySide.QtCore import Qt
from PySide.QtGui import QCursor

from threedee import collision
from entity import *
#find out if this is really needed


class Director(object):
    """
    Directs the Actors to act according to some kind of script which may be based on user input.
    The actors are part of the scene and identified as actors by their description in the script.
    Makes the actors execute according to script.    (script interpreter)
    """
    def __init__(self, scene, script):
        self.scene      = scene
        self.script     = script
        
        #keys and buttons that shall be able to be pressed continuosly
        self.key_filter = set([Qt.Key_Left, Qt.Key_Right, Qt.Key_Down, Qt.Key_Up,
                               Qt.Key_Space, Qt.Key_PageDown, Qt.Key_PageUp,  
                               Qt.Key_J, Qt.Key_I, Qt.Key_O, Qt.Key_U, Qt.Key_L, Qt.Key_K, 
                               Qt.Key_W, Qt.Key_A, Qt.Key_S, Qt.Key_D, Qt.Key_Q, Qt.Key_E,
                               Qt.Key_Y, Qt.Key_X, Qt.Key_C
                               ]
                              )
        
        self.button_filter = set([Qt.LeftButton, Qt.MidButton, Qt.RightButton])
        
        self.is_down    = set()
        self.mousePos = [0,0]
        self.mousePos2 = [0]
        
    def key_down(self, event):
        key = event.key()
        
        if key == Qt.Key_N:
            self.scene.hud.shownet = not self.scene.hud.shownet
            
        if key == Qt.Key_B:
            self.scene.blocksvisible = not self.scene.blocksvisible
            self.scene.view.updateGL() 
        

        if key == Qt.Key_Control:
            pass
  
        if key in self.key_filter:
            self.is_down.add(key)
        
    def key_up(self, event):
        key = event.key()
        
        if key == Qt.Key_Control:
            pass
            
        self.is_down.discard(key)
        

    def mouse_down(self, event):
        button = event.button()
    
        if button in self.button_filter:
            self.is_down.add(button)    
    
    def mouse_up(self, event):             
        button = event.button()
        
        self.is_down.discard(button)    

    def mouse_move(self, event):        #not trust on that, look for other things
        pos = event.pos()
        #x_,y_  = self.mousePos
        #dx, dy = pos.x()-x_, -(pos.y()-y_)
        #rint dx, dy
        #if pos.x() == 400 or pos.y() == 400:
        #    event.ignore()
        #    #rint "xxx"
        #    return
        #player = self.scene.player
        #player.rotate(0, amount = 1.5*dx)
        #player.rotate(2, amount = 1.5*dy)

        self.mousePos = [pos.x(),pos.y()]
        if QCursor.pos().x() > 600 or QCursor.pos().x() < 200 or QCursor.pos().y() > 600 or QCursor.pos().y() < 200:
            #print type(QCursor.pos().x())
            QCursor.setPos(400,400)

  
    def animation_step(self, t):
        balance = 0.5
        rotation_angle  = 5     * balance   # would be part of the ships actor?
        strafe          = 1.5   * balance

        player          = self.scene.player
        pp = player.pos
        delta_rotation  = 0      
            
        
        if len(self.mousePos) == 2 and len(self.mousePos2) == 2:
            dx, dy = (self.mousePos2[0]-self.mousePos[0], self.mousePos2[1]-self.mousePos[1])
            if abs(dx) > 150 or abs(dy) > 150:
                pass
            else:
                if dx != 0:
                    player.rotate(0, amount = 0.5*dx)
                if dy != 0:
                    player.rotate(1, amount = -0.5*dy)
                #QCursor.setPos(400,400)
        
        self.mousePos2 = (self.mousePos[0], self.mousePos[1])
        mp = self.mousePos
        adjmouse = mousepos_to_gamepos(mp,pp)
        
        
        lclick = Qt.LeftButton in self.is_down
        if lclick:
            pass
       
            
            
        rclick = Qt.RightButton in self.is_down
        if rclick: 
            pass
        
        if Qt.Key_Y in self.is_down:
            player.rotate(2, amount = 0.5)
        if Qt.Key_X in self.is_down:
            player.rotate(2, amount = -0.5)
        
        if Qt.Key_U in self.is_down:
            if self.scene.fov < 170:
                self.scene.fov += 1
        if Qt.Key_J in self.is_down:
            if self.scene.fov > 10:
                self.scene.fov -= 1
            


        
        '''
        if self.scene.time%(4*self.scene.tick_duration):
            
            rotateL = [Qt.Key_U in self.is_down, Qt.Key_I in self.is_down, Qt.Key_O in self.is_down]
            rotateR = [Qt.Key_J in self.is_down, Qt.Key_K in self.is_down, Qt.Key_L in self.is_down] 
            for i in range (3):
                if rotateR[i] != rotateL[i]:      #if front or back and not nothing or front&back
                    if rotateR[i]:
                        player.rotate(i, amount = 1)
                    else:
                        player.rotate(i, amount = -1)
        '''            
                
        right   = Qt.Key_D in self.is_down
        left    = Qt.Key_A in self.is_down
        if right != left:      #if front or back and not nothing or front&back
            if right:
                #player.change_pos(*[0.5,0,0])
                player.move_side(-0.01)
            else:
                #player.change_pos(*[-0.5,0])
                player.move_side(0.01)

        up      = Qt.Key_W in self.is_down
        down    = Qt.Key_S in self.is_down
        if up != down:      #if front or back and not nothing or front&back
            if up:
                #player.change_pos(*[0,0.5,0])
                player.move_forward(0.01)
            else:
                #player.change_pos(*[0,-0.5,0])
                player.move_forward(-0.01)
                
        front  = Qt.Key_Q in self.is_down
        back   = Qt.Key_E in self.is_down
        if front != back:      #if front or back and not nothing or front&back
            if front:
                #player.change_pos(*[0,0,0.5])
                player.move_up(0.01)
            else:
                #player.change_pos(*[0,0,-0.5])
                player.move_up(-0.01)
        
class Actor(object):
    """
    provides actions, acted out according to Script, directed by the Director.
    
    TODO
    reason about different types of actions, and how they can be "told" by the director
    timing etc.
    """
    pass

    
class Script(object):
    """
    Describes a set of entities and how they act upon another
    """
    pass

