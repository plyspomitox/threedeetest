# Game: "Codename: ef" - ef
# Author: Plyspomitox (Contact: @xoryps)
# License: not resolved yet
#
#    plys

import sys
import os.path
def relative_path(p):       return os.path.join(os.path.dirname(os.path.abspath(__file__)), p)

src_path = relative_path('./src/')



if __name__ == "__main__":
    if len(sys.argv) >= 2 and sys.argv[1] == "PYTHONPATH":
        print src_path 
    else:
        init_file = relative_path('.init.local')
        if not os.path.exists(init_file):
            from subprocess import call
            code = call(['sh', 'export_path.sh', '&&', 'exit'])
            if code:
                print "failed with exit code " + unicode(code)
            else:
                print "successfully initialized PYTHONPATH"
                print "now your code completion should stop complaining ;-)"


        sys.path.insert(0, src_path)
        from threedee.main import main
        main(sys.argv)


