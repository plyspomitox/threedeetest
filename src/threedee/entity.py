# Game: "Colored Sources in Space" - csis
# Author: Plyspomitox (Contact: @xoryps)
# License: CC-by-sa
#
#

import math
import random

from threedee.opengl import *
from threedee.constants import colors, IC, RD
from threedee.world import Entity
from threedee.vector import *
from threedee.utility import mousepos_to_gamepos, gamepos_to_mousepos, distance

import collision


class Area(Entity):
    def __init__(self, vertices = [], texture = None, color = None):
        super(Area, self).__init__()
        self.vertices = vertices
        self.texture = texture
        if self.texture is not None:
            self.scene.texturemap[self.texture].append(self)
        if color == None:
            self.color = random.choice(colors.ALLCOLORS)
        else:
            self.color = color

    def draw_main(self,viewport = None):
        p = self.scene.player
        ppos = self.scene.player.pos
        
        glLoadIdentity()
        glScale(0.01,0.01,0.01)
        #glTranslate(-ppos[0],-ppos[1],-ppos[2])
        #glRotated(p.angle[0] / -1.0, 1.0, 0.0, 0.0)        #x
        #glRotated(p.angle[1] / -1.0, 0.0, 1.0, 0.0)        #y
        #glRotated(p.angle[2] / -1.0, 0.0, 0.0, 1.0)        #z
        
        glBegin(GL_POLYGON)
        glColor3ub(*self.color)
        for i in self.vertices:
            glVertex3d(*i)
        glEnd()

class Block(Entity):
    def __init__(self, verticeAreas = []):
        super(Block, self).__init__()
        self.active = True
        self.verticeAreas = verticeAreas
        
    def add_vertice_area(self, area):
        self.verticeAreas.append(area)

    def draw_main(self,viewport = None):
        for i in self.verticeAreas:
            i.draw_main(viewport)
 

class Map(Entity):
    CHUNKSIZE = 40
    def __init__(self):
        super(Map, self).__init__()
        self.themap = {}
        
    def init_map(self,x1,x2,y1,y2):
        for i in range(x1/self.CHUNKSIZE, x2/self.CHUNKSIZE):
            if i not in self.themap.keys():
                self.themap[i] = {}
            for j in range(y1/self.CHUNKSIZE, y2/self.CHUNKSIZE):
                fe = FloorElement(random.randint(0,2), self.CHUNKSIZE*i, self.CHUNKSIZE*j)
                self.themap[i][j] = fe 
                self.scene.add(fe)
    
    def generate_chunk(self,x,y):
        pass
        
    def draw_main(self,viewport):
        for i in self.themap:
            for j in self.themap[i]:
                self.themap[i][j].draw_main(viewport)

class FloorElement(Entity):
    CHUNKSIZE = 40
    def __init__(self, kind,x,y):
        super(FloorElement, self).__init__()
        self.pos = [x,y]    
        self.kind = kind
    
    def draw_main(self, viewport):  
        #glLoadIdentity()    
        ppos = self.scene.player.pos
        pos = self.pos
        
        CHUNKSIZE = self.CHUNKSIZE
        glLoadIdentity()
        glScale(0.01,0.01,0.01)
        glTranslate(-ppos[0],-ppos[1],0)
        
        glBegin(GL_QUADS)
        glColor3ub(25 if self.kind == 0 else 0, 25 if self.kind == 1 else 0, 25 if self.kind == 2 else 0)
        glVertex3d(pos[0]+CHUNKSIZE, pos[1]+CHUNKSIZE,0)
        glVertex3d(pos[0], pos[1]+CHUNKSIZE,0)
        glVertex3d(pos[0], pos[1],0)
        glVertex3d(pos[0]+CHUNKSIZE, pos[1],0)
        glEnd()

   

class Player(Entity):
    '''
    Is silly and flies around
    '''

    def __init__(self):
        super(Player, self).__init__()

        self.color = [255,0,100]
        self.hud = 0
        #self.values = {'pos': [0,0,0], 'angle': [0,0,0], 'dirvec': [1,0,0], 'headvec':[0,1,0]}
        self.pos = [0,0,0]
        self.angle = [0,0,0]
        self.dirvec = [1,0,0]
        self.headvec = [0,0,1]
        self.sidevec = vec_cross_vec(self.headvec, self.dirvec)
        self.speed = 0.1
        
        #self.rotbase = (self.headvec,self.sidevec, self.dirvec)

    def change_pos(self,x,y,z):
        self.pos[0] += x
        self.pos[1] += y
        self.pos[2] += z
        
        #rint self.pos

    def move_forward(self, distance):
        #self.pos[0] += self.dirvec[0]*distance
        #self.pos[1] += self.dirvec[1]*distance
        #self.pos[2] += self.dirvec[2]*distance
        self.change_pos(*scale_vec(distance, self.dirvec))
        
    def move_side(self, distance):
        self.change_pos(*scale_vec(distance, self.sidevec))
    
    def move_up(self, distance):
        self.change_pos(*scale_vec(distance, self.headvec))



    def rotate_2(self, dim = 2, amount = 5):
        self.angle[dim]+=amount

    def rotate(self, dim = 0, amount = 1):
        (x,y,z) = [self.headvec, self.sidevec, self.dirvec][dim]
        
        ca = math.cos(math.radians(amount))
        sa = math.sin(math.radians(amount))
        mca = 1.0-ca
        #rint x,y,z

        rotmat = [[x*x*mca + ca,      x*y*mca - z*sa,   x*z*mca + y*sa ],
                  [y*x*mca + z*sa,    y*y*mca + ca,     y*z*mca - x*sa ],
                  [z*x*mca - y*sa,    z*y*mca + x*sa,   z*z*mca + ca ]
                  ]
        
        #[self.headvec, self.sidevec, self.dirvec] = mat_mult_mat(rotmat, [self.headvec, self.sidevec, self.dirvec])
        [self.headvec, self.sidevec, self.dirvec] = transpose(mat_mult_mat(rotmat, transpose([self.headvec, self.sidevec, self.dirvec])))



    def draw_main(self, viewport):
        glLoadIdentity()
        glScale(0.01,0.01,0.01)

        #cargo
        glBegin(GL_LINE_LOOP)
        glColor3ub(*self.color)
        glVertex2d(2,2)
        glVertex2d(-2,2)
        glVertex2d(-2,-2)
        glVertex2d(2,-2)        
        glEnd()        
        
