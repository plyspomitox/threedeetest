"""
This module contains collision test functions.
Note:
The naming convention used is lowerdimensional and less complex objects are named first.
""" 
from __future__ import division

import math

#def point_point(x1,y1,x2,y2,dist = 0.001):  # this kind of is circle circle 
#    return math.pow(x1-x2,2) + math.pow(y1-y2,2) < math.pow(dist, 2)     # if within circle


def point_point(p1, p2, dist = 0.001):  # this kind of is circle circle 
    return math.pow(p1[0]-p2[0], 2) + math.pow(p1[1]-p2[1], 2) < math.pow(dist, 2)     # if within circle

def point_point_alt(p1, p2, vec):
    return math.pow(p1[0]-p2[0], 2) + math.pow(p1[1]-p2[1], 2) <= math.pow(vec[0], 2) + math.pow(vec[1], 2)


    
#how to determine the direction in which the point may/not pass the line
#the line has to be given in mathematical positive direction, outside is on the right side    
def point_line(px1,py1,lx1,ly1,lx2,ly2):
    pass


def line_line(l1x1,l1y1,l1x2,l1y2,l2x1,l2y1,l2x2,l2y2, already_used=0):
    if l2x2==l2x1: 
        if already_used:
            return False 
        else:
            return line_line(l1y1,l1x1,l1y2,l1x2,l2y1,l2x1,l2y2,l2x2, 1)
    
    n =  float( ((l1y2-l1y1)*(l2x2-l2x1)-(l1x2-l1x1)*(l2y2-l2y1)))
    if (n==0): return False
    
    r = float( ( (l1x1-l2x1)*(l2y2-l2y1)+(l2y1-l1y1)*(l2x2-l2x1) )/n)
    s = float(( l1x1 - l2x1 + r*(l1x2-l1x1) )/(l2x2-l2x1))
    #hier muss noch was eingebaut werden fuer den fall dass l2x2==l2x1, zb vergleich der y werte und umgekehrte behandlung
    #if (r>=0 and r<=1 and s>=0 and s<=1): print 'katze'
    
    return (r>=0 and r<=1 and s>=0 and s<=1)


def line_circle():
    #lotgerade zum Kreismittelpunkt mit zur gerade verlaengerten strecke schneiden lassen
    #und dann gucken ob der Schnittpunkt auf der Strecke liegt. wenn nicht, 
    #dann weiter suchen auf der gerade
    pass

def circle_in_square(pt, sqpt, sqlen, dist):
    #if pt[0]-sqpt[0] ==
    
    if(     (sqpt[0]+sqlen/2<=pt[0] or sqpt[0]-sqlen/2>=pt[0]) 
        and (sqpt[1]+sqlen/2<=pt[1] or sqpt[1]-sqlen/2>=pt[1])):
        return True
    

    #return math.pow(pt[0]-sq[0], 2) + math.pow(pt[1]-sq[1], 2) <= math.pow(dist, 2)














