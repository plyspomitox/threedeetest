

#important constants
#should make all those constants with cap letters
class IC(object):
    
    ENERGYFORSHOT = 35
    
    
    spawnwidth = 900
    neededcollisioncolor = 127 #153
    amountmonsters = 22
    MONSTERSUPPLIES = 12
    PLANTSUPPLIES = 5
    MOBSHOOTRANGE = 90
    MOBFOLLOWRANGE = 50
    TIME_IN_SECOND = 100
    netwidth = 25
    POINTLIFE = 42
    pointspeed = 0.1
    start_resources = 5000
    xpneeded = {}
    for i in range(20):
        xpneeded[i] = (10*i)**2
    maxmanaperlevel = {}
    for i in range(20):
        maxmanaperlevel[i] = int((1.2**(i-1))*255)
    maxhealthperlevel = {}
    for i in range(20):
        maxhealthperlevel[i] = int((1.2**(i-1))*255)
    manaregperlevel = {}
    for i in range(20):
        manaregperlevel[i] = i
    healthregperlevel = {}
    for i in range(20):
        healthregperlevel[i] = i
    
    regshowlist = [[i,10] for i in range(10) ] + \
            [[10,-i+10] for i in range(20)] + \
            [[-i+10,-10] for i in range(20)] + \
            [[-10,i-10] for i in range(20)] + \
            [[i-10,10] for i in range(11) ]
        
    ITEMNAMES = {0: "Poisonous Liana"
            ,1: "Liquid Sunlight"
            ,2: "Wood" 
            ,3: "Ice"
            ,4: "Poison Element"
            ,5: "Poison Seed"
             }    



class colors(object):
    white   = (255, 255, 255)
    black   = (0,0,0)
    red     = (255, 0, 0)
    green   = (0, 255, 0)
    blue    = (0, 0, 255)    
    magenta = (255,0,255)
    cyan    = (0,255,255)
    yellow  = (255,255,0)
    grey    = (127,127,127)

    ALLCOLORS = [white, red, green, blue, yellow, magenta, cyan, grey]    
    
#ResourceDictionary
RD = {0: ["red", colors.red], 1: ["green", colors.green], 2: ["blue", colors.blue],
      3: ["metal", colors.cyan], 4: ["lifeform", colors.magenta], 5: ["sand", colors.yellow], 
      6: ["gold", (255,127,0)], 7: ["water", (102,127,255)], 8: ["glass", (200,200,200)],
      9: ["carbon", (50,50,0)], 10: ["ice", (0,127,255)], 11: ["elecboard", (0,255,127)],
      12: ["salt", (0,127,127)], 13: ["halogene", (127,127,0)], 14: ["acid", (50,200,50)],
      15: ["energy", (204,204,204)]  }
        

#rhythmium, #optium, #melodium, #colorium (randomly colorchanging stuff) 
#magnetic-metal? Salt, Halogenes
#Gold?Platinum? waver?
#stuff that is fused with (in Fuser) colors can get magic or other special properties, #Seperator
#antivacuum
# if a consciousness dies(partly), then lifeforms(and parts of its upgrades) are dropped 


#buildingplans for certain things/or certain important parts shall already be vorhanden at the start, 
#i.e. the central part of the energy generator (a triangular struct which lets through all three colors)


