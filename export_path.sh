﻿#!/usr/bin/env bash

# find directory
SITEDIR=$(python -c 'import site; site._script()' --user-site)

# create if it doesn't exist
echo "The site-packages dir is:"
echo $SITEDIR
mkdir -p $SITEDIR

# create new .pth file with our path
python threedee_start.py PYTHONPATH > $SITEDIR/threedee.pth


# create init marker file
echo "This file is used so that we know whether we initialized the PYTHONPATH already." > .init.local

# some parts of this setup could be considered a little bit over the top
# but one has to experiment...

# implementing this part in python might be worth it 

exit
