# Game: "Secret Project" - ef
# Author: Plyspomitox (Contact: @xoryps)
# License: ####
#
#

import random
from datetime import date, time, datetime
from time import gmtime, strftime



from PySide.QtCore import QTimer, Qt
from PySide.QtGui import QApplication, QWidget, QDesktopWidget, QPushButton, QTextEdit

from threedee import collision
from threedee.constants import * #IC, colors
from threedee.view import Viewport, MainPerspective
from threedee.world import Scene
from threedee.entity import * # FloorElement, Player, Monster, Item, Projectile
from threedee.animation import Director
from threedee.utility import trim
        
import math
#needs updating when new classes come

#just add this as an attribute to scene. like self.scene.sched
class PlysScheduler(object):
    def __init__(self, timevar):
        self.sched = {}
        self.timevar = timevar
        self.lasttime = 0
        
    def add(self, delay, func, args):
        if self.timevar+delay in self.sched:
            self.sched[self.timevar+delay].append([func, args])
            #rint self.sched
        else:
            self.sched[self.timevar+delay] = [[func, args],]
        
        #rint self.sched
        
    def execute(self):
        for i in range(self.lasttime, self.timevar):
            if i in self.sched:
                for func, args in self.sched[i]:
                    #import pdb; pdb.set_trace()
                    #for obj, func, args in entries:
                            
                    if args == None:
                        func()
                    else:
                        func(*args)
        
        #self.iterate()
        self.remove_outdated()
        
    #def iterate(self):
    #    self.timevar += 1
    
    def set_time(self, t):
        self.lasttime, self.timevar = self.timevar, t    
    
    def search_next(self, func, args):
        for i, v in self.sched.iteritems():
            if [func, args] in v:
                return i
            
    def time_till(self, func, args):
        x = self.search_next(func, args)
        if x is not None:
            return x - self.timevar
        
    #remove a certain entry
    def remove_next(self, func, args):
        for i, v in self.sched.iteritems():
            if [func, args] in v:
                v.remove([func, args])
                break
                
    def remove_outdated(self):
        for i in self.sched.keys():
            if i < self.lasttime:
                #rint self.sched
                del self.sched[i]
    
class Game(QWidget):
    time = 0
    
    def keyPressEvent(self, event):     self.director.key_down(event)
    def keyReleaseEvent(self, event):   self.director.key_up(event)
    def mousePressEvent(self, event):   self.director.mouse_down(event)
    def mouseReleaseEvent(self, event): self.director.mouse_up(event)
    def mouseMoveEvent(self, event):    self.director.mouse_move(event)
    
    def __init__(self, parent=None):
        super(Game, self).__init__(parent)
        self.setWindowTitle(self.tr("ef"))
        #self.setGeometry(0,0,1005,705)


        tempdesktop = QApplication.desktop() 
        
        self.maxWidth = (tempdesktop.screenGeometry().width()/100)*100
        self.maxHeight = (tempdesktop.screenGeometry().height()/100)*100
        
        #self.mainlayout = QGridLayout(self)


        self.viewwidth = 1.6*self.maxHeight #1200
        self.viewheight = 0.8*self.maxHeight # 800
        
        if self.viewheight > self.maxWidth:
            self.viewwidth = self.maxWidth                                                  #layoutfolgen bedenken
        
        #self.setGeometry(0,0,self.viewwidth,self.viewheight)

        self.setMouseTracking(True)  
        #not maximize
        
        self.scene      = Scene()
        self.director   = Director(self.scene, None)
        
        self.setFocusPolicy(Qt.StrongFocus)      
        
        self.scene.fov = 90.0
        self.scene.nearend = 0.02
        self.scene.farend = 10.0
        
        self.main_view = Viewport(MainPerspective(self.scene), self)
        self.main_view.setGeometry(2,2,800,800)
        #self.main_view.setMouseTracking(True)
        #        self.scene.add(self.main_view, 'main_view')       
        
        
        self.scene.texturemap = {}
        
        self.main_view.setMouseTracking(True)
        self.main_view.grabKeyboard()
        self.scene.view = self.main_view
        # start timer
        self.scene.time = 0
        
        self.scene.blocksvisible = True
        #self.add_block()
        self.add_block_line()
        self.add_room()
        
        
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.timestep)

        self.scene.sched = PlysScheduler(self.scene.time)


        self.scene.tick_duration = 5
        self.timer.start(self.scene.tick_duration)    
        self.seconds_per_timestep = self.scene.tick_duration / 1000.0
        
        #self.scene.add(Floor(), 'floor')
        self.scene.add(Player(), 'player')


    def add_block_line(self):
        for i in range(16):
            self.add_block([10*i+4,4,4]) #,[55+10*i,0,0])
    
    def add_wireframe(self):
        pass
    
    def add_room(self):
        #walls
        self.add_block([0,0,0], _sizes = [500,3,500])
        self.add_block([0,500,0], _sizes = [500,3,500])
        self.add_block([0,0,0], _sizes = [3,500,500])
        self.add_block([500,0,0], _sizes = [3,500,500])
        self.add_block([0,0,0], _sizes = [500,500,3])
        self.add_block([0,0,500], _sizes = [500,500,3])    
        
        #table
        self.add_block([200,5,0], _sizes = [5,5,150])    
        self.add_block([400,5,0], _sizes = [-5,5,150])
        self.add_block([400,155,0], _sizes = [-5,-5,150])
        self.add_block([200,155,0], _sizes = [5,-5,150])
        self.add_block([200,5,150], _sizes = [200,150,5])
        
    
    def add_block(self, _pos = [0,0,0], col = None, _sizes = [5,15,20]):
        sizes = _sizes
        pos = _pos
        
        vts = [[pos[0], pos[1], pos[2]],
               [pos[0]+sizes[0], pos[1], pos[2]],
               [pos[0]+sizes[0], pos[1]+sizes[1], pos[2]],
               [pos[0], pos[1]+sizes[1], pos[2]],
               [pos[0], pos[1], pos[2]+sizes[2]],
               [pos[0]+sizes[0], pos[1], pos[2]+sizes[2]],
               [pos[0]+sizes[0], pos[1]+sizes[1], pos[2]+sizes[2]],
               [pos[0], pos[1]+sizes[1], pos[2]+sizes[2]]
               ]
        
        areas = [ Area([ vts[0], vts[1], vts[2], vts[3] ], color = col),
                  Area([ vts[4], vts[5], vts[6], vts[7] ], color = col),
                  Area([ vts[0], vts[4], vts[5], vts[1] ], color = col),
                  Area([ vts[3], vts[7], vts[6], vts[2] ], color = col),
                  Area([ vts[0], vts[4], vts[7], vts[3] ], color = col),
                  Area([ vts[1], vts[5], vts[6], vts[2] ], color = col)
                 ]
        for a in areas:
            self.scene.add(a)
        
        x = Block(areas)
        self.scene.add(x)






    def build_map(self):
        m = Map()
        self.scene.add(m)
        m.init_map(-240,240,-240,240)



    def timestep(self):
        self.scene.time += self.scene.tick_duration
        self.scene.sched.set_time(self.scene.time)
        self.timedHappenings()

    FORMAT_STRING = trim("""
        x: %(x)i
        y: %(y)i
        Active weapon: %(w)s
        (Press T to toggle)
        Active tool: %(t)s
        (Press Q to toggle)
        Killed: %(k)i
        Mobs in Game: %(m)i
        Items in Game: %(i)i
        Projectile in Game: %(p)i
        
        Health: %(h)i
        XP: %(xp)i
        Level: %(lvl)i
        
        INVENTORY:
        """)

    def timedHappenings(self):
        self.director.animation_step(self.scene.time)
        self.collision_test()
        if self.scene.time%(2*self.scene.tick_duration) == 0:
            self.main_view.updateGL()
        
        p = self.scene.player
        ppos = p.pos

 
        seconds = self.scene.time * self.seconds_per_timestep 
        

        self.scene.sched.execute()
        
     
    def collision_test(self):
        
        p = self.scene.player
        

    def make_image(self):
        image = self.main_view.grabFrameBuffer()
        #renderToTexture

        #strftime("%Y-%m-%d %H:%M:%S", gmtime())
        image.save("newpic"+strftime("%Y-%m-%d %H-%M-%S", gmtime())+".png")


