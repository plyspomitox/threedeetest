"""
Main module of this package call main with commandline parameters (which get passed to Qt)
or execute the csis application module which does this for you.
"""

import sys
from PySide.QtGui   import QApplication
from threedee.game import Game

def main(argv):
    app = QApplication(argv)
    game = Game()
    game.show()

    
    
    sys.exit(app.exec_())
    
    
