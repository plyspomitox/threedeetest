# Game: "Colored Sources in Space" - csis
# Author: Plyspomitox (Contact: @xoryps)
# License: CC-by-sa
#
#
import math

from PySide.QtOpenGL import QGLWidget
from threedee.opengl import *
from threedee.constants import colors, IC
from threedee.entity import * # Floor, Character, Item
from PySide.QtGui import QFont
from PySide.QtCore import QRect
#from Image import *
from PIL import Image

# Number of the glut window.
window = 0

# Rotations for cube. 
xrot = yrot = zrot = 0.0

fogColor=(0.0, 0.0, 0.0, 0.0) # fog color

texture = 0

def draw_area(viewport, area):
    p = area.scene.player
    ppos = area.scene.player.pos
    
    glLoadIdentity()
    glScale(0.01,0.01,0.01)
    
    glBegin(GL_POLYGON)
    glColor3ub(*area.color)
    for i in area.vertices:
        glVertex3d(*i)
    glEnd()




def vec_to_str(vec):
    s = "{:.4f}   "*len(vec)
    s = s.format(*vec)
    return s
       
#rint vec_to_str([1.11111, 2.34567, 100.3])
    

class Viewport(QGLWidget):
    def __init__(self, perspective, parent=None):
        super(Viewport, self).__init__(parent)
        self.perspective = perspective
    
    def initializeGL(self):     pass    
    def resizeGL(self, w, h):   self.perspective.viewport_resized(w, h)
    def paintGL(self):          self.perspective.draw(self)        
    
            
class MainPerspective(object):
    def __init__(self, scene):
        self.scene = scene
    
    def setup(self, width = 100, height = 100):
        
        #self.texture_import()
        #glEnable(GL_TEXTURE_2D)

        glLoadIdentity()
        #glScale(0.01,0.01,0.01)
        p = self.scene.player
        ppos = p.pos
        t = self.scene.time
        #glRotated(player.angle / -1.0, 0.0, 0.0, 1.0)
        #glTranslate(-ppos[0], -ppos[1],0)

        glClearColor(0.0, 0.0, 0.0, 0.0)    # This Will Clear The Background Color To Black
        glClearDepth(1.0)                    # Enables Clearing Of The Depth Buffer
        glDepthFunc(GL_LESS)                # The Type Of Depth Test To Do
        glEnable(GL_DEPTH_TEST)                # Enables Depth Testing
        glShadeModel(GL_SMOOTH)                # Enables Smooth Color Shading
        
        glMatrixMode(GL_PROJECTION)
        #glLoadIdentity()                    # Reset The Projection Matrix
        glLoadIdentity()                                    # Calculate The Aspect Ratio Of The Window
        #glFrustum(-10,10,-10,10,5,50) 
        
        gluPerspective(self.scene.fov, 
                       float(width)/float(height),
                       self.scene.nearend, 
                       self.scene.farend
                       )
        #eye, pos, lookingdirection     #WRONG!
        #eye, center, up -> aka -> position, direction, head
        #gluLookAt(0,0,2, 0,0,0, 0,1,0)
        '''
        gluLookAt(0,0,2,
                  3*math.cos(math.radians(t)),3*math.sin(math.radians(t)),0, 
                  0,1,0 
                  )
        '''
        #factor1 = 0.1
        center = add_vec(p.pos,p.dirvec)
        gluLookAt(p.pos[0], p.pos[1], p.pos[2], 
                  center[0], center[1], center[2],
                  *p.headvec )
        
        
        
        #math.cos(math.radians(t))
        #gluPerspective(90, 1, 10, 100)
        glMatrixMode(GL_MODELVIEW)



        
    def viewport_resized(self, width, height):    
        if height == 0:                        # Prevent A Divide By Zero If The Window Is Too Small 
            height = 1
        #rint width, height
        side = min(width, height)
        glViewport((width - side) / 2, (height - side) / 2, side, side)
        #rint (width - side) / 2, (height - side) / 2, side, side


        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        #glOrtho(-100, 100, -100, 100, -100.0, 100.0);
        
        #glViewport(0, 0, Width, Height)        # Reset The Current Viewport And Perspective Transformation
        glMatrixMode(GL_MODELVIEW)
        #glLoadIdentity()
        #gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
        #glMatrixMode(GL_MODELVIEW)
        
        
        
    def draw(self, viewport):
        self.setup()
        
        # scene draw stuff 
        glClearColor(0, 0, 0, 0)
         
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  
      
        #if self.scene.hud.shownet:
        #    self.buildnet(IC.netwidth) 
        
        p = self.scene.player
        #self.draw_gl_scene()
        
        #rint self.scene.floor
        #self.scene.floor.draw_main(viewport)
       
        #rint self.scene.get_list(Item)
       
        for a in self.scene.iter_instances(Player): 
            a.draw_main(viewport)  

        
        if self.scene.blocksvisible:
            for c in self.scene.iter_instances(Block): 
                if c.active:
                    c.draw_main(viewport)
                    


        glColor3ub(127,51,153)
        posit = [gamepos_to_mousepos([60+p.pos[0],80-10*i+p.pos[1]],p.pos) for i in range(9)]
        #viewport.renderText(posit[0][0], posit[0][1], str(p.angle))
        viewport.renderText(posit[1][0], posit[1][1], vec_to_str(p.pos))
        viewport.renderText(posit[2][0], posit[2][1], vec_to_str(p.dirvec))
        viewport.renderText(posit[3][0], posit[3][1], vec_to_str(p.headvec))
        viewport.renderText(posit[4][0], posit[4][1], vec_to_str(p.sidevec))
        v = [vec_length(i) for i in [p.dirvec, p.headvec, p.sidevec]]
        viewport.renderText(posit[5][0], posit[5][1], vec_to_str(v))
        crossvec = vec_cross_vec(p.headvec, p.dirvec)
        viewport.renderText(posit[6][0], posit[6][1], vec_to_str(crossvec))
        vsp = [vec_mult_vec(i,j) for i,j in zip([p.dirvec, p.headvec, p.sidevec],[p.headvec, p.sidevec, p.dirvec])]
        viewport.renderText(posit[7][0], posit[7][1], vec_to_str(vsp))
        viewprefs = [self.scene.fov, self.scene.nearend, self.scene.farend]
        viewport.renderText(posit[8][0], posit[8][1], vec_to_str(viewprefs))        
        
        #p.pos, p.dirvec


        #self.scene.hud.draw_main(viewport)

        #self.draw_some_textures(viewport)

    def fill_textures(self):
        pass



    def draw_some_textures(self, viewport):
        glEnable( GL_TEXTURE_2D )
        
        img = Image.open("img/Katzenente.jpg")
    
        #img = QtGui.QPixmap(":/img/Katzenente.jpg")
        ix = img.size[0]
        iy = img.size[1]
        img = img.tostring("raw", "RGBX", 0, -1)
        
        #glBindTexture( GL_TEXTURE_2D, image )
        #viewport.bindTexture( img )
        glBindTexture(GL_TEXTURE_2D, glGenTextures(1))   # 2d texture (x and y size)
        
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        glTexImage2D(GL_TEXTURE_2D, 0, 3, ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, img)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
        
        glBegin( GL_QUADS )
        a = 1.0
        size = 10
        
        glTexCoord2f(0.0, 0.0); glVertex3f(-size/2, -size/2,  1.0)    # Bottom Left Of The Texture and Quad
        glTexCoord2f(a, 0.0); glVertex3f( size/2, -size/2,  1.0)    # Bottom Right Of The Texture and Quad
        glTexCoord2f(a, a); glVertex3f( size/2,  size/2,  1.0)    # Top Right Of The Texture and Quad
        glTexCoord2f(0.0, a); glVertex3f(-size/2,  size/2,  1.0)        
        glEnd()
        glDisable( GL_TEXTURE_2D )


        glEnable( GL_TEXTURE_2D )
        
        img = Image.open("img/C.jpg")
    
        #img = QtGui.QPixmap(":/img/Katzenente.jpg")
        ix = img.size[0]
        iy = img.size[1]
        img = img.tostring("raw", "RGBX", 0, -1)
        
        #glBindTexture( GL_TEXTURE_2D, image )
        #viewport.bindTexture( img )
        glBindTexture(GL_TEXTURE_2D, glGenTextures(1))   # 2d texture (x and y size)
        
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        glTexImage2D(GL_TEXTURE_2D, 0, 3, ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, img)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
        
        glBegin( GL_QUADS )
        a = 1.0
        size = 14
        move = 15
        
        glTexCoord2f(0.0, 0.0); glVertex3f(-size/2+move, -size/2,  1.0)    # Bottom Left Of The Texture and Quad
        glTexCoord2f(a, 0.0); glVertex3f( size/2+move, -size/2,  1.0)    # Bottom Right Of The Texture and Quad
        glTexCoord2f(a, a); glVertex3f( size/2+move,  size/2,  1.0)    # Top Right Of The Texture and Quad
        glTexCoord2f(0.0, a); glVertex3f(-size/2+move,  size/2,  1.0)        
        glEnd()
        glDisable( GL_TEXTURE_2D )
        
        
    def texture_import(self):
        image = open("img/green_low.png")
        
        ix = image.size[0]
        iy = image.size[1]
        image = image.tostring("raw", "RGBX", 0, -1)
        
        # Create Texture    
        glBindTexture(GL_TEXTURE_2D, glGenTextures(1))   # 2d texture (x and y size)
        
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        glTexImage2D(GL_TEXTURE_2D, 0, 3, ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, image)
        #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
        #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
        




    def draw_gl_scene(self):
        global xrot, yrot, zrot, texture
    
        #glutInit(sys.argv)
    
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)    # Clear The Screen And The Depth Buffer
        glLoadIdentity()                    # Reset The View
        
        glClearColor(0.0,0.0,0.0,1.0)            
        
        glTranslatef(0.0,0.0,-5.0)            # Move Into The Screen
    
        glRotatef(xrot,1.0,0.0,0.0)            # Rotate The Cube On It's X Axis
        glRotatef(yrot,0.0,1.0,0.0)            # Rotate The Cube On It's Y Axis
        glRotatef(zrot,0.0,0.0,1.0)            # Rotate The Cube On It's Z Axis
        
        # Note there does not seem to be support for this call.
        #glBindTexture(GL_TEXTURE_2D, texture)    # Rotate The Pyramid On It's Y Axis
    
        glBegin(GL_QUADS)                # Start Drawing The Cube
        #glBegin(GL_TRIANGLES)                # Start Drawing The Cube
        
        
        # Front Face (note that the texture's corners have to match the quad's corners)
        glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0,  1.0)    # Bottom Left Of The Texture and Quad
        glTexCoord2f(2.5, 0.0); glVertex3f( 1.0, -1.0,  1.0)    # Bottom Right Of The Texture and Quad
        glTexCoord2f(2.5, 1.0); glVertex3f( 1.0,  1.0,  1.0)    # Top Right Of The Texture and Quad
        glTexCoord2f(0.0, 1.0); glVertex3f(-1.0,  1.0,  1.0)    # Top Left Of The Texture and Quad
        
        # Back Face
        glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0, -1.0)    # Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0, 1.0); glVertex3f(-1.0,  1.0, -1.0)    # Top Right Of The Texture and Quad
        glTexCoord2f(0.0, 1.0); glVertex3f( 1.0,  1.0, -1.0)    # Top Left Of The Texture and Quad
        glTexCoord2f(0.0, 0.0); glVertex3f( 1.0, -1.0, -1.0)    # Bottom Left Of The Texture and Quad
        
        # Top Face
        glTexCoord2f(0.0, 1.0); glVertex3f(-1.0,  1.0, -1.0)    # Top Left Of The Texture and Quad
        glTexCoord2f(0.0, 0.0); glVertex3f(-1.0,  1.0,  1.0)    # Bottom Left Of The Texture and Quad
        glTexCoord2f(1.0, 0.0); glVertex3f( 1.0,  1.0,  1.0)    # Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0, 1.0); glVertex3f( 1.0,  1.0, -1.0)    # Top Right Of The Texture and Quad
        
        # Bottom Face       
        glTexCoord2f(1.0, 1.0); glVertex3f(-1.0, -1.0, -1.0)    # Top Right Of The Texture and Quad
        glTexCoord2f(0.0, 1.0); glVertex3f( 1.0, -1.0, -1.0)    # Top Left Of The Texture and Quad
        glTexCoord2f(0.0, 0.0); glVertex3f( 1.0, -1.0,  1.0)    # Bottom Left Of The Texture and Quad
        glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0,  1.0)    # Bottom Right Of The Texture and Quad
        
        # Right face
        glTexCoord2f(1.0, 0.0); glVertex3f( 1.0, -1.0, -1.0)    # Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0, 1.0); glVertex3f( 1.0,  1.0, -1.0)    # Top Right Of The Texture and Quad
        glTexCoord2f(0.0, 1.0); glVertex3f( 1.0,  1.0,  1.0)    # Top Left Of The Texture and Quad
        glTexCoord2f(0.0, 0.0); glVertex3f( 1.0, -1.0,  1.0)    # Bottom Left Of The Texture and Quad
        
        # Left Face
        glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, -1.0)    # Bottom Left Of The Texture and Quad
        glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0,  1.0)    # Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0, 1.0); glVertex3f(-1.0,  1.0,  1.0)    # Top Right Of The Texture and Quad
        glTexCoord2f(0.0, 1.0); glVertex3f(-1.0,  1.0, -1.0)    # Top Left Of The Texture and Quad
        
        glEnd();                # Done Drawing The Cube
        
        xrot = xrot + 0.1                 # X rotation
        yrot = yrot + 0.1                 # Y rotation
        zrot = zrot + 0.1                 # Z rotation
    
        #  since this is double buffered, swap the buffers to display what just got drawn. 
        #glutSwapBuffers()
    

    def buildnet(self, dist):
        p = self.scene.player  
        
        
        ix = dist*int(p.pos[0] / dist)
        iy = dist*int(p.pos[1] / dist)
        
        glBegin(GL_LINES)
        glColor3ub(15,15,15)
        for netx in range(-10,+11):
            for nety in range(-10,+11):
                glVertex3d( ix+dist*(+netx), iy+dist*(-nety),1)
                glVertex3d( ix+dist*(+netx), iy+dist*(+nety),1)
              
                glVertex3d( ix+dist*(-netx), iy+dist*(+nety),1)
                glVertex3d( ix+dist*(+netx), iy+dist*(+nety),1)
        glEnd()
    
        
            
