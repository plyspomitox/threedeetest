
import math
import itertools


def get_vector(a, b):
    if isinstance(a, Vector2D):
        return a
    elif isinstance(b, Vector2D):
        return b
    else:
        raise ValueError("both not a vector")
    
def get_scalar(a, b):
    if not isinstance(a, Vector2D):     # TODO test for a scalar instead?? probably not...
        return a
    elif not isinstance(b, Vector2D):
        return b
    else:
        raise ValueError("both not a vector")

def scalar_times_vector(a, b):  #,type_)
    if not isinstance(a,Vector3D) and isinstance(b,Vector3D):
        return a,b
    elif isinstance(a,Vector3D) and not isinstance(b,Vector3D):
        return b,a
    #return both, in correct order
    else:
        raise ValueError("both not a vector")

class Vector2D(object):
    def __init__(self, *args):
        if len(args) == 1:
            if isinstance(args[0], Vector2D):
                self.x, self.y = args[0].x, args[0].y       # TODO refactor
            else:
                self.x, self.y = args[0]
        else:
            self.x, self.y = args
                
    def __add__(self, other):
        return Vector2D(self.x+other.x, self.y+other.y)
    
    def __sub__(self, other):
        return Vector2D(self.x-other.x, self.y-other.y)
    
    def __mul__(self, other):       # TODO implement vector vector multiplication or use a library 
        vector = get_vector(self, other)
        scalar = get_scalar(self, other)
        return Vector2D(vector.x*scalar, vector.y*scalar)
    
    def __div__(self, other):
        vector = get_vector(self, other)
        scalar = get_scalar(self, other)
        scalar = 1/scalar
        return Vector2D(vector.x*scalar, vector.y*scalar)
    
    def length(self):
        return math.sqrt(self.x**2 + self.y**2)
    
    def normal(self):
        return Vector2D(self.y, -self.x)
    
    def unit(self):
        #returns the unit vector
        return self / self.length()
    
    def squared_distance(self):
        return self.x**2 + self.y**2


class Vector3D(object):
    def __init__(self, *args):
        if len(args) == 1:
            if isinstance(args[0], Vector3D):
                self.x, self.y, self.z = args[0].x, args[0].y, args[0].z       # TODO refactor
            else:
                self.x, self.y, self.z = args[0]
        else:
            self.x, self.y, self.z = args
                
    def __add__(self, other):
        return Vector3D(self.x+other.x, self.y+other.y, self.z+other.z)
    
    def __sub__(self, other):
        return Vector3D(self.x-other.x, self.y-other.y, self.z-other.z)
    
    def __mul__(self, other):       # TODO implement vector vector multiplication or use a library 
        if isinstance(other, Vector3D):
            return self.x*other.x + self.y*other.y + self.z*other.z
        else:
            scalar, vector = scalar_times_vector(self, other)        
            return Vector3D(scalar*vector.x, scalar*vector.y, scalar*vector.z)
    def __rmul__(self,other):
        scalar, vector = scalar_times_vector(self, other)        
        return Vector3D(scalar*vector.x, scalar*vector.y, scalar*vector.z)
    
    def __pow__(self,other):
        if isinstance(other, Vector3D):
            return Vector3D(self.y*other.z - self.z*other.y,
                            self.z*other.x - self.x*other.z, 
                            self.x*other.y - self.y*other.x)
    
    '''
    def __div__(self, other):
        vector = get_vector(self, other)
        scalar = get_scalar(self, other)
        scalar = 1/scalar
        return Vector2D(vector.x*scalar, vector.y*scalar)
    '''
            
    def __str__(self):
        return "("+str(self.x)+","+str(self.y)+","+str(self.z)+")"
    
    def length(self):
        return math.sqrt(self.x**2 + self.y**2)
    
    #def normal(self):
    #    return Vector2D(self.y, -self.x)
    
    def to_list(self):
        return [self.x, self.y, self.z]
    
    def unit(self):
        #returns the unit vector
        return self / self.length()
    
    def squared_distance(self):
        return self.x**2 + self.y**2    
    


#TODO: do something so that either Vector3D is used or just lists

class Matrix3D(object):
    
    def __init__(self, *args):

        #if len(args) == 1:    
        if len(args) == 3:
        #the vectors get to be the rows
            self.m = [Vector3D(args[0]), Vector3D(args[1]), Vector3D(args[2])]
            
        
        elif len(args) == 9:
        #the vectors get to be the rows

            self.m = [Vector3D(args[0:3]), Vector3D(args[3:6]), Vector3D(args[6:9])]
    
    
    #def __get__(self):
    
    
    #seems to be exactly a transpose
    def get_colmat(self):
        #return [[self.m[0],self.m[3],self.m[6]],[self.m[1],self.m[4],self.m[7]],[self.m[2],self.m[5],self.m[8]]]
        return [ Vector3D(self.m[0].x,self.m[1].x,self.m[2].x),
                 Vector3D(self.m[0].y,self.m[1].y,self.m[2].y),
                 Vector3D(self.m[0].z,self.m[1].z,self.m[2].z)
                ]
    
    
    def multvec(self, vec):
        return Vector3D( self.m[0]*vec,
                         self.m[1]*vec,
                         self.m[2]*vec
                         )
    
    #doesn't work correctly, transposes the product
    #UPDATE: now works, but surely it can be better
    def multmat(self, mat):
        m2 = mat.get_colmat()
        
        
        return Matrix3D(*Matrix3D(self.multvec(m2[0]).to_list(),
                        self.multvec(m2[1]).to_list(),
                        self.multvec(m2[2]).to_list() 
                        ).get_colmat())


v1 = [3,4,5,6]
v2 = [4,3,2,1]
m1 = [[4,3,2,1],[1,2,3,4],[5,5,5,5],[0,1,0,1]]
mi = [[1,0,0,0],[0,2,0,0],[0,0,3,0],[0,0,0,4]]
m2 = [[1,2,0,0],[0,2,3,0],[0,0,3,4],[1,0,0,4]]

def transpose(mat):
    return [list(i) for i in itertools.izip(*mat)]

def plys_vec_str(vec):
    s = str(vec[0])
    for i in vec[1:]:
        s+= "  " + str(i)
    return s

def plys_mat_str(mat):
    s = plys_vec_str(mat[0])
    for i in mat[1:]:
        s+= "\r" + plys_vec_str(i)
    return s

#print plys_vec_str([4,5,3,4,5,1])
#print plys_mat_str([[4,5,3],[4,5,1]])

def scale_vec(scale, vec):
    return [scale*i for i in vec]

def add_vec(vec1, vec2):
    return [i+j for i, j in itertools.izip(vec1, vec2)]

def sub_vec(vec1, vec2):
    return [i-j for i, j in itertools.izip(vec1, vec2)]

def vec_length(vec):
    return math.sqrt(reduce( lambda x,y: x+y, (i**2 for i in vec)))

def vec_mult_vec(vec1, vec2):
    return reduce(lambda a, b: a+b,[v_1*v_2 for v_1, v_2 in itertools.izip(vec1, vec2)])
    
def vec_cross_vec(vec1, vec2):
    return [vec1[1]*vec2[2]-vec1[2]*vec2[1],
            vec1[2]*vec2[0]-vec1[0]*vec2[2],
            vec1[0]*vec2[1]-vec1[1]*vec2[0]
            ]

def mat_mult_vec(mat, vec):
    return [vec_mult_vec(v_i, vec) for v_i in mat]

def mat_mult_mat(mat1, mat2):
    return transpose([mat_mult_vec(mat1, v_i) for v_i in transpose(mat2)])
    #pass
    
    
#rint vec_mult_vec(v1, v2) 
#rint "\n"
#rint plys_vec_str(mat_mult_vec(m1, v2)) 
#rint "\n"
#rint plys_mat_str(mat_mult_mat(m1, mi))



#TestArea
'''
a = Vector3D(1,0,0)
b = Vector3D(3,0,0)
c = 7
print 'a = ', a
print 'b = ', b
print 'c = ', c

print "a*b = ", a*b
print "b*a = ", b*a
print "a x b = ", a**b
print "a*c = ", a*c
print "c*a = ", c*a
print "b*c = ", b*c
print "c*b = ", c*b
'''
'''
a = Matrix3D(1,0,0,
             4,0,0,
             7,0,0
             )
b = Matrix3D(1,2,3,
             0,0,0,
             0,0,0
             )

c = a.multmat(b)

print a.m[0].x, a.m[0].y, a.m[0].z
print a.m[1].x, a.m[1].y, a.m[1].z
print a.m[2].x, a.m[2].y, a.m[2].z
print ""
print b.m[0].x, b.m[0].y, b.m[0].z
print b.m[1].x, b.m[1].y, b.m[1].z
print b.m[2].x, b.m[2].y, b.m[2].z
print ""
print c.m[0].x, c.m[0].y, c.m[0].z
print c.m[1].x, c.m[1].y, c.m[1].z
print c.m[2].x, c.m[2].y, c.m[2].z
print ""
'''


