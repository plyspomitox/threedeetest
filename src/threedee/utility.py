import random, math

class ignore(object):
    def __init__(self, exception_type):             self.exception_type = exception_type
    def __enter__(self):                            return self
    def __exit__(self, exc_type, exc_val, exc_tb):  return exc_type == self.exception_type


def trim(string):
    lines = string.split("\n")
    lines = [line.lstrip() for line in lines]

    with ignore(IndexError):
        while not lines[0]:
            lines.pop(0)

    with ignore(IndexError):
        while not lines[-1]:
            lines.pop()
        
    return "\n".join(lines)

halfscreen = 400
scale_ = halfscreen/100.0

def gamepos_to_mousepos(pos, playerpos):
    return  [(pos[0]-playerpos[0])*scale_ + halfscreen
                  ,(pos[1]-playerpos[1])*(-scale_) + halfscreen
                  ]

def mousepos_to_gamepos(pos, playerpos):
    return  [playerpos[0]+(pos[0]-halfscreen)/scale_,
                         playerpos[1]-(pos[1]-halfscreen)/scale_]
    
def distance(pos1, pos2):
    return math.sqrt((pos1[0]-pos2[0])**2+(pos1[1]-pos2[1])**2)



    